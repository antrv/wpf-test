using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using TaskManager.Models;
using TaskManager.Services;
using TaskManager.ViewModels;
using Xunit;

namespace TaskManager.Tests
{
    public class MainWindowServiceTests : IDisposable
    {
        private readonly MainWindowViewModel _viewModel;
        private readonly IWindowListService _windowListService;
        private readonly IProcessListService _processListService;
        private readonly IStateStorageService<MainWindowState> _stateStorageService;
        private readonly MainWindowService _service;

        public MainWindowServiceTests()
        {
            _viewModel = new MainWindowViewModel();
            _windowListService = Substitute.For<IWindowListService>();
            _processListService = Substitute.For<IProcessListService>();
            _stateStorageService = Substitute.For<IStateStorageService<MainWindowState>>();

            // Mocking basic behavior
            _windowListService.GetTopWindowList().Returns(new List<WindowModel>());
            _processListService.GetProcesses().Returns(new List<ProcessModel>());

            // Service under test
            _service = new MainWindowService(_viewModel, _stateStorageService, _windowListService, _processListService);
        }

        public void Dispose()
        {
            _service.Dispose();
        }

        [Fact]
        public void Initialize_CommandsMustBeSet()
        {
            // arrange

            // act
            _service.Initialize();

            // assert
            _viewModel.ExitCommand.Should().NotBeNull();
            _viewModel.KillProcessCommand.Should().NotBeNull();
            _viewModel.RunCommand.Should().NotBeNull();
            _viewModel.SwitchToAppCommand.Should().NotBeNull();
        }

        [Fact]
        public void Initialize_LoadsState()
        {
            // arrange
            MainWindowState expectedState = new();
            _stateStorageService.LoadState().Returns(expectedState);

            // act
            _service.Initialize();

            // assert
            _stateStorageService.Received(1).LoadState();
            _viewModel.State.Should().BeSameAs(expectedState);
        }

        [Fact]
        public async Task TimerCallbackCalledAtLeastOnce()
        {
            // arrange

            // act
            _service.Initialize();
            await Task.Delay(TimeSpan.FromMilliseconds(10)); // Yield thread

            // assert
            _processListService.Received().GetProcesses();
            _windowListService.Received().GetTopWindowList();
        }
    }
}
