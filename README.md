# This is the Homework task.

## Program Requirements

The program requires Windows 7-10. The program is written using .NET 5 and the latest C# 9. The program requires .NET 5 SDK installed to compile. 
There are no other requirements.

To run the program, do the following steps:

- Install the latest [.NET 5 SDK for Windows](https://dotnet.microsoft.com/download/dotnet/5.0)
- Install [GIT](http://git-scm.com/download/win) for Windows.
- Create a folder, i.e. `TaskManager` somewhere.
- Run a command line and change the current directory to the directory, created at the previous step.
- Clone the repository using the following command:

  > `git clone https://antrv@bitbucket.org/antrv/wpf-test.git .`

- Build and execute the program using the following command:

  > `dotnet run -p TaskManager -c Release`

---

## Functionality

![The screenshot](screenshot.png)

- The main menu contains the `Run command` item, that allows to run applications with optional arguments.
- Double-click on an application row in the applications table brings the selected application to the foreground.
- The context menu is available on the right mouse click in both the application and process table. It allows to terminate the selected process.
- Hot keys are assigned for running a command (ALT-R) and exiting the program (ALT-X).
- The application and process tables are refreshed with the period of approximately 0.7 seconds.
- The application and process tables support sorting and column reordering.
- The state of the main window (width, height) is saved at exit and is restored at the next launch.
- I has made all strings localizable. For example I added the Finnish localization for the program name (main window caption). So the program displays main window caption in Finnish being launched in PC with Finnish Windows UI language. The translation may be not accurate because I don't know Finnish and I used Google translate.
- I implemented only few unit tests to demonstrate the knowlegde.

---

## Improvement Points

- The application and process tables state (columns width and order, sort order) can also be saved at exit and restored at the next launch.
- Some service classes has been not well designed and could be decoupled into separate services. This prevents writing well-detailed unit tests also.
- The possible code improvements are commented with `TODO`'s in the code. 

---

## Issues

- Icon, memory and cpu information are not available for system processes. Probably the Windows task manager uses other API to get this information, or it has more privileges.
- There are some ghost applications shown in the list that are not displayed in the task bar, for example `Settings` and `Microsoft Store`. I don't know the conditions to filter them.
