﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TaskManager.Converters
{
    public sealed class MemoryValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is long longValue)
            {
                return GetMemoryString(longValue);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotSupportedException();

        private static string GetMemoryString(long memory)
        {
            const long kb = 1L << 10;
            const long mb = 1L << 20;
            const long gb = 1L << 30;
            const long tb = 1L << 40;

            return memory switch
            {
                >= tb => Math.Round((decimal)memory / tb, 1) + " TB",
                >= gb => Math.Round((decimal)memory / gb, 1) + " GB",
                >= mb => Math.Round((decimal)memory / mb, 1) + " MB",
                >= kb => Math.Round((decimal)memory / kb, 1) + " KB",
                _ => memory + " B"
            };
        }
    }
}
