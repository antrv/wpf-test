﻿using System;
using System.Windows;
using Microsoft.Extensions.DependencyInjection;
using TaskManager.Localization;
using TaskManager.Services;
using TaskManager.ViewModels;
using TaskManager.Views;

namespace TaskManager
{
    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            try
            {
                ServiceCollection serviceCollection = new ServiceCollection();
                RegisterServices(serviceCollection);
                using ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
                Run(serviceProvider);
            }
            catch (Exception exception)
            {
                string message = string.Format(LocalizationResources.ErrorOccured, exception.Message);
                MessageBox.Show(message, LocalizationResources.ErrorCaption, MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private static void RegisterServices(IServiceCollection serviceCollection)
        {
            // Main application
            serviceCollection.AddSingleton<App>();

            // Windows and view models
            serviceCollection.AddSingletonWindow<MainWindow, MainWindowViewModel>();

            // Services
            serviceCollection.AddSingleton<IWindowListService, WindowListService>();
            serviceCollection.AddSingleton<IProcessListService, ProcessListService>();
            serviceCollection.AddSingleton<IMainWindowService, MainWindowService>();
            serviceCollection.AddSingleton(typeof(IStateStorageService<>), typeof(StateStorageService<>));
        }

        private static void Run(IServiceProvider serviceProvider)
        {
            App app = serviceProvider.GetRequiredService<App>();
            MainWindow mainWindow = serviceProvider.GetRequiredService<MainWindow>();

            IMainWindowService mainWindowService = serviceProvider.GetRequiredService<IMainWindowService>();
            mainWindowService.Initialize();

            app.Run(mainWindow);
        }
    }
}
