﻿using System;
using System.Windows.Media.Imaging;

namespace TaskManager.Models
{
    public sealed class ProcessModel
    {
        public int ProcessId { get; set; }

        public string Name { get; set; }

        public int Threads { get; set; }

        public BitmapSource Icon { get; set; }

        public long? Memory { get; set; }

        // Resources
        public DateTime TimeStamp { get; set; }

        public TimeSpan CpuTime { get; set; }
    }
}
