﻿namespace TaskManager.Models
{
    public sealed class WindowModel
    {
        public nint WindowHandle { get; set; }

        public string Name { get; set; }

        public int ProcessId { get; set; }
    }
}
