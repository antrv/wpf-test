﻿using System;
using System.Windows.Input;
using JetBrains.Annotations;

namespace TaskManager.Models
{
    public sealed class DelegateCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Func<object, bool> _canExecute;

        public DelegateCommand([NotNull] Action execute, [CanBeNull] Func<bool> canExecute = null)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            _execute = parameter => execute();
            _canExecute = canExecute is null ? static parameter => true : parameter => canExecute();
        }

        public DelegateCommand([NotNull] Action<object> execute, [CanBeNull] Func<object, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute ?? (static parameter => true);
        }

        [NotNull]
        public static ICommand Create([NotNull] Action execute, [CanBeNull] Func<bool> canExecute = null) =>
            new DelegateCommand(execute, canExecute);

        [NotNull]
        public static ICommand Create([NotNull] Action<object> execute,
            [CanBeNull] Func<object, bool> canExecute = null) =>
            new DelegateCommand(execute, canExecute);

        public bool CanExecute(object parameter) => _canExecute(parameter);

        public void Execute(object parameter) => _execute(parameter);

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}
