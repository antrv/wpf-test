﻿using System;
using System.Windows;
using TaskManager.ViewModels;

namespace TaskManager.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowClosed(object sender, EventArgs e)
        {
            // TODO: System.Window.Interactivity library can be used for the MVVM approach.
            if (DataContext is IExitCommandProvider exitCommandProvider)
            {
                exitCommandProvider.ExitCommand?.Execute(null);
            }
        }
    }
}
