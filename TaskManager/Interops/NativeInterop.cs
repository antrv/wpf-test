﻿using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace TaskManager.Interops
{
    internal sealed class NativeInterop
    {
        /// <summary>
        /// https://docs.microsoft.com/en-us/previous-versions/windows/desktop/legacy/ms633498(v=vs.85)
        /// </summary>
        [return: MarshalAs(UnmanagedType.Bool)]
        internal delegate bool EnumWindowProc(nint hwnd, nint lParam);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-enumwindows
        /// </summary>
        [DllImport("User32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool EnumWindows(EnumWindowProc lpEnumFunc, nint lParam);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getwindowtextw
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpString"></param>
        /// <param name="nMaxCount"></param>
        /// <returns></returns>
        [DllImport("User32")]
        internal static extern int GetWindowText(nint hWnd, [Out] StringBuilder lpString, int nMaxCount);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getwindowinfo
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="pwi"></param>
        /// <returns></returns>
        [DllImport("User32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowInfo(nint hwnd, ref WindowInfo pwi);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getwindowthreadprocessid
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpdwProcessId"></param>
        /// <returns></returns>
        [DllImport("User32")]
        internal static extern uint GetWindowThreadProcessId(nint hWnd, out uint lpdwProcessId);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-getmodulefilenameexa
        /// </summary>
        /// <param name="hProcess"></param>
        /// <param name="hModule"></param>
        /// <param name="lpFilename"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        internal static extern int GetModuleFileNameEx(nint hProcess, nint hModule,
            [Out] StringBuilder lpFilename, int nSize);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-getmodulefilenameexa
        /// </summary>
        /// <param name="hProcess"></param>
        /// <param name="hModule"></param>
        /// <param name="lpFilename"></param>
        /// <param name="nSize"></param>
        /// <returns></returns>
        [DllImport("Psapi", EntryPoint = nameof(GetModuleFileNameEx) + "W")]
        internal static extern int GetModuleFileNameExPsApi(nint hProcess, nint hModule,
            [Out] StringBuilder lpFilename, int nSize);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/nf-tlhelp32-createtoolhelp32snapshot
        /// </summary>
        /// <param name="dwFlags"></param>
        /// <param name="th32ProcessID"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        internal static extern nint CreateToolhelp32Snapshot(uint dwFlags, uint th32ProcessID);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle
        /// </summary>
        /// <param name="hObject"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool CloseHandle(nint hObject);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/nf-tlhelp32-process32first
        /// </summary>
        /// <param name="hSnapshot"></param>
        /// <param name="lppe"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool Process32FirstW(nint hSnapshot, ref ProcessEntry lppe);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/nf-tlhelp32-process32next
        /// </summary>
        /// <param name="hSnapshot"></param>
        /// <param name="lppe"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool Process32NextW(nint hSnapshot, ref ProcessEntry lppe);

        /// <summary>
        /// https://docs.microsoft.com/ru-ru/windows/win32/api/processthreadsapi/nf-processthreadsapi-getprocesstimes
        /// </summary>
        /// <param name="hProcess"></param>
        /// <param name="lpCreationTime"></param>
        /// <param name="lpExitTime"></param>
        /// <param name="lpKernelTime"></param>
        /// <param name="lpUserTime"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetProcessTimes(nint hProcess, out FILETIME lpCreationTime, out FILETIME lpExitTime,
            out FILETIME lpKernelTime, out FILETIME lpUserTime);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-openprocess
        /// </summary>
        /// <param name="dwDesiredAccess"></param>
        /// <param name="bInheritHandle"></param>
        /// <param name="dwProcessId"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        internal static extern nint OpenProcess(uint dwDesiredAccess,
            [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-getprocessmemoryinfo
        /// </summary>
        /// <param name="processHandle"></param>
        /// <param name="counters"></param>
        /// <param name="cb"></param>
        /// <returns></returns>
        [DllImport("Kernel32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetProcessMemoryInfo(nint processHandle, ref ProcessMemoryCountersEx counters,
            int cb);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/psapi/nf-psapi-getprocessmemoryinfo
        /// </summary>
        /// <param name="processHandle"></param>
        /// <param name="counters"></param>
        /// <param name="cb"></param>
        /// <returns></returns>
        [DllImport("Psapi", EntryPoint = nameof(GetProcessMemoryInfo))]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetProcessMemoryInfoPsApi(nint processHandle, ref ProcessMemoryCountersEx counters,
            int cb);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setforegroundwindow
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("User32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool SetForegroundWindow(nint hWnd);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="nCmdShow"></param>
        /// <returns></returns>
        [DllImport("User32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool ShowWindow(nint hWnd, int nCmdShow);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getwindowplacement
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="lpwndpl"></param>
        /// <returns></returns>
        [DllImport("User32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowPlacement(nint hWnd, ref WindowPlacement lpwndpl);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-queryfullprocessimagenamew
        /// </summary>
        /// <param name="hProcess"></param>
        /// <param name="dwFlags"></param>
        /// <param name="lpExeName"></param>
        /// <param name="lpdwSize"></param>
        /// <returns></returns>
        [DllImport("Kernel32", CharSet = CharSet.Auto)]
        internal static extern bool QueryFullProcessImageName(nint hProcess, uint dwFlags, StringBuilder lpExeName,
            ref uint lpdwSize);

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/windef/ns-windef-rect
        /// </summary>
        internal struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/previous-versions/dd162805(v=vs.85)
        /// </summary>
        internal struct Point
        {
            public int X;
            public int Y;
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-windowinfo
        /// </summary>
        internal struct WindowInfo
        {
            public uint Size;
            public Rect Window;
            public Rect Client;
            public uint Style;
            public uint ExStyle;
            public uint WindowStatus;
            public uint XWindowBorders;
            public uint YWindowBorders;
            public ushort WindowType;
            public ushort CreatorVersion;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct Array4<T>
            where T : unmanaged
        {
            public T Value0;
            public T Value1;
            public T Value2;
            public T Value3;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct Array16<T>
            where T : unmanaged
        {
            public Array4<T> Value0;
            public Array4<T> Value1;
            public Array4<T> Value2;
            public Array4<T> Value3;
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/ns-tlhelp32-processentry32w
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct ProcessEntry
        {
            internal const int MAX_PATH = 260;

            public int Size;
            public uint Usage;
            public int ProcessId;
            public nint DefaultHeapId;
            public uint ModuleId;
            public int Threads;
            public uint ParentProcessId;
            public int PriClassBase;
            public uint Flags;

            // This is fixed array of length MAX_PATH of chars, i.e.
            // public fixed char ExeFile[MAX_PATH];
            // I don't want to use unsafe context.
            public Array16<Array16<char>> ExeFile; // 256 characters
            public Array4<char> ExeFileContinue; // + 4 characters
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/psapi/ns-psapi-process_memory_counters_ex
        /// </summary>
        internal struct ProcessMemoryCountersEx
        {
            public int cb;
            public uint PageFaultCount;
            public nint PeakWorkingSetSize;
            public nint WorkingSetSize;
            public nint QuotaPeakPagedPoolUsage;
            public nint QuotaPagedPoolUsage;
            public nint QuotaPeakNonPagedPoolUsage;
            public nint QuotaNonPagedPoolUsage;
            public nint PagefileUsage;
            public nint PeakPagefileUsage;
            public nint PrivateUsage;
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/winuser/ns-winuser-windowplacement
        /// </summary>
        internal struct WindowPlacement
        {
            public int Length;
            public uint Flags;
            public uint ShowCmd;
            public Point MinPosition;
            public Point MaxPosition;
            public Rect NormalPosition;
            public Rect Device;
        }
    }
}
