﻿using System.Collections.Generic;
using TaskManager.Models;

namespace TaskManager.Services
{
    public interface IWindowListService
    {
        List<WindowModel> GetTopWindowList();
    }
}
