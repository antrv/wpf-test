﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using TaskManager.Interops;
using TaskManager.Models;

namespace TaskManager.Services
{
    internal sealed class ProcessListService : IProcessListService
    {
        private readonly Dictionary<string, BitmapSource> _iconCache = new(StringComparer.InvariantCultureIgnoreCase);

        public List<ProcessModel> GetProcesses()
        {
            List<ProcessModel> result = new();

            NativeInterop.ProcessEntry processEntry = default;
            processEntry.Size = Marshal.SizeOf<NativeInterop.ProcessEntry>();

            // https://docs.microsoft.com/en-us/windows/win32/api/tlhelp32/nf-tlhelp32-createtoolhelp32snapshot
            const uint TH32CS_SNAPPROCESS = 0x00000002;

            nint handle = NativeInterop.CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
            try
            {
                if (NativeInterop.Process32FirstW(handle, ref processEntry))
                {
                    result.Add(ConvertToModel(ref processEntry));
                    while (NativeInterop.Process32NextW(handle, ref processEntry))
                    {
                        result.Add(ConvertToModel(ref processEntry));
                    }
                }
            }
            catch
            {
                // TODO: log exception
            }
            finally
            {
                NativeInterop.CloseHandle(handle);
            }

            return result;
        }

        private ProcessModel ConvertToModel(ref NativeInterop.ProcessEntry processEntry)
        {
            ProcessModel model = new();

            model.ProcessId = processEntry.ProcessId;

            ReadOnlySpan<char> exeNameSpan = MemoryMarshal.CreateReadOnlySpan(
                ref processEntry.ExeFile.Value0.Value0.Value0.Value0,
                NativeInterop.ProcessEntry.MAX_PATH);

            int terminationChar = exeNameSpan.IndexOf('\0');
            if (terminationChar < 0)
            {
                terminationChar = exeNameSpan.Length + 1;
            }

            model.Name = new string(exeNameSpan[..terminationChar]);

            model.TimeStamp = DateTime.Now;
            model.Threads = processEntry.Threads;

            // https://docs.microsoft.com/en-us/windows/win32/procthread/process-security-and-access-rights
            const int PROCESS_QUERY_INFORMATION = 0x400;

            nint processHandle = NativeInterop.OpenProcess(PROCESS_QUERY_INFORMATION,
                false, processEntry.ProcessId);
            if (processHandle != 0 && processHandle != nint.MinValue) // MinValue is Invalid Handle
            {
                try
                {
                    if (NativeInterop.GetProcessTimes(processHandle, out _, out _, out FILETIME kernelTime,
                        out FILETIME userTime))
                    {
                        model.CpuTime = GetTimeSpan(kernelTime) + GetTimeSpan(userTime);
                    }

                    model.Memory = GetProcessWorkingSet(processHandle);

                    model.Icon = GetIconForProcess(processHandle);
                }
                finally
                {
                    NativeInterop.CloseHandle(processHandle);
                }
            }

            return model;
        }

        private static TimeSpan GetTimeSpan(FILETIME time)
        {
            long longTime = (((long)time.dwHighDateTime << 32) + time.dwLowDateTime) / Environment.ProcessorCount;
            return TimeSpan.FromTicks(longTime);
        }

        private static nint GetProcessWorkingSet(nint processHandle)
        {
            NativeInterop.ProcessMemoryCountersEx counters = default;
            counters.cb = Marshal.SizeOf<NativeInterop.ProcessMemoryCountersEx>();

            try
            {
                if (NativeInterop.GetProcessMemoryInfoPsApi(processHandle, ref counters, counters.cb))
                {
                    return counters.WorkingSetSize;
                }
            }
            catch (EntryPointNotFoundException)
            {
                if (NativeInterop.GetProcessMemoryInfo(processHandle, ref counters, counters.cb))
                {
                    return counters.WorkingSetSize;
                }
            }

            return 0;
        }

        private BitmapSource GetIconForProcess(nint processHandle)
        {
            StringBuilder sb = new(2048);
            uint sbSize = 2048;
            if (!NativeInterop.QueryFullProcessImageName(processHandle, 0, sb, ref sbSize))
            {
                return null;
            }

            string fileName = sb.ToString(0, (int)sbSize);

            if (_iconCache.TryGetValue(fileName, out BitmapSource imageSource))
            {
                return imageSource;
            }

            using Icon icon = Icon.ExtractAssociatedIcon(fileName);
            if (icon != null)
            {
                Dispatcher dispatcher = Application.Current?.Dispatcher;
                if (dispatcher == null)
                {
                    return null;
                }

                imageSource = dispatcher.Invoke(() => Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions()));

                _iconCache[fileName] = imageSource;
            }

            return imageSource;
        }
    }
}
