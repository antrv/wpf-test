﻿using System.Collections.Generic;
using TaskManager.Models;

namespace TaskManager.Services
{
    public interface IProcessListService
    {
        List<ProcessModel> GetProcesses();
    }
}
