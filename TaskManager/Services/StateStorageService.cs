﻿using System.IO;
using System.Text;
using System.Text.Json;

namespace TaskManager.Services
{
    internal sealed class StateStorageService<TState> : IStateStorageService<TState>
        where TState : class
    {
        private readonly string _fileName = typeof(TState).Name + ".state.json";

        public TState LoadState()
        {
            if (File.Exists(_fileName))
            {
                try
                {
                    string json = File.ReadAllText(_fileName, Encoding.UTF8);
                    return JsonSerializer.Deserialize<TState>(json);
                }
                catch
                {
                    // TODO: log exception
                }
            }

            return null;
        }

        public void SaveState(TState state)
        {
            try
            {
                File.WriteAllText(_fileName, JsonSerializer.Serialize(state));
            }
            catch
            {
                // TODO: log exception
            }
        }
    }
}
