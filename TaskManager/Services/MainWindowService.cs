﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using JetBrains.Annotations;
using TaskManager.Interops;
using TaskManager.Localization;
using TaskManager.Models;
using TaskManager.ViewModels;
using TaskManager.Views;
using Utils = TaskManager.Utilities.Utils;

namespace TaskManager.Services
{
    internal sealed class MainWindowService : IMainWindowService, IDisposable
    {
        private readonly TimeSpan _refreshInterval = TimeSpan.FromMilliseconds(700);
        private readonly MainWindowViewModel _viewModel;
        private readonly IStateStorageService<MainWindowState> _stateStorageService;
        private readonly IWindowListService _windowListService;
        private readonly IProcessListService _processListService;
        private readonly Timer _timer;
        private volatile bool _timerReentranceGuard;

        public MainWindowService([NotNull] MainWindowViewModel viewModel,
            [NotNull] IStateStorageService<MainWindowState> stateStorageService,
            [NotNull] IWindowListService windowListService,
            [NotNull] IProcessListService processListService)
        {
            _viewModel = viewModel ?? throw new ArgumentNullException(nameof(viewModel));
            _stateStorageService = stateStorageService ?? throw new ArgumentNullException(nameof(stateStorageService));
            _windowListService = windowListService ??
                throw new ArgumentNullException(nameof(windowListService));

            _processListService = processListService ?? throw new ArgumentNullException(nameof(processListService));
            _timer = new Timer(TimerCallback, null, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }

        public void Initialize()
        {
            _viewModel.State = _stateStorageService.LoadState() ?? new MainWindowState();

            // It is better to use weak references here (for example
            // Reactive UI library provides implementation of weak references, events and commands).
            _viewModel.ExitCommand = DelegateCommand.Create(ExitExecute);
            _viewModel.RunCommand = DelegateCommand.Create(RunExecute);
            _viewModel.SwitchToAppCommand = DelegateCommand.Create(SwitchToAppExecute);
            _viewModel.KillProcessCommand = DelegateCommand.Create(KillProcessExecute);

            // Start updating lists
            _timer.Change(TimeSpan.Zero, _refreshInterval);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
            _timer.Dispose();
        }

        private void TimerCallback(object _)
        {
            // TODO: It is difficult to test the timer callback method.
            // TODO: This functionality must be extracted to a separate service.

            if (_timerReentranceGuard)
            {
                return;
            }

            _timerReentranceGuard = true;
            try
            {
                RefreshLists();
            }
            catch
            {
                // TODO: log exception
            }
            finally
            {
                _timerReentranceGuard = false;
            }
        }

        private void RefreshLists()
        {
            // This method is executed in the separate thread.
            List<ProcessModel> processes = _processListService.GetProcesses();
            List<WindowModel> windows = _windowListService.GetTopWindowList();

            Dispatcher dispatcher = Application.Current?.Dispatcher;
            if (dispatcher is null || dispatcher.CheckAccess())
            {
                UpdateLists(processes, windows);
            }
            else
            {
                dispatcher.Invoke(() => UpdateLists(processes, windows));
            }
        }

        private void UpdateLists(List<ProcessModel> processes, List<WindowModel> windows)
        {
            // applications
            int index = 0;
            while (index < _viewModel.Applications.Count)
            {
                ApplicationViewModel model = _viewModel.Applications[index];
                WindowModel window = windows.Find(x => x.WindowHandle == model.WindowHandle);
                if (window is null)
                {
                    _viewModel.Applications.RemoveAt(index);
                    continue;
                }

                UpdateApplication(model, window);
                ProcessModel process = processes.Find(x => x.ProcessId == window.ProcessId);
                if (process is not null)
                {
                    model.Icon = process.Icon;
                }

                windows.Remove(window);
                index++;
            }

            foreach (WindowModel window in windows)
            {
                ApplicationViewModel model = new();
                UpdateApplication(model, window);

                ProcessModel process = processes.Find(x => x.ProcessId == window.ProcessId);
                if (process is not null)
                {
                    model.Icon = process.Icon;
                }

                _viewModel.Applications.Add(model);
            }

            // processes
            index = 0;
            while (index < _viewModel.Processes.Count)
            {
                ProcessViewModel model = _viewModel.Processes[index];
                ProcessModel process = processes.Find(x => x.ProcessId == model.ProcessId);
                if (process is null)
                {
                    _viewModel.Processes.RemoveAt(index);
                    continue;
                }

                UpdateProcess(model, process, model);
                processes.Remove(process);
                index++;
            }

            foreach (ProcessModel process in processes)
            {
                ProcessViewModel model = new();
                UpdateProcess(model, process);
                _viewModel.Processes.Add(model);
            }
        }

        private static void UpdateApplication(ApplicationViewModel model, WindowModel window)
        {
            model.ProcessId = window.ProcessId;
            model.Name = window.Name;
            model.WindowHandle = window.WindowHandle;
        }

        private static void UpdateProcess(ProcessViewModel model, ProcessModel process,
            ProcessViewModel oldModel = null)
        {
            model.ProcessId = process.ProcessId;
            model.Name = process.Name;
            model.Icon = process.Icon;
            model.Threads = process.Threads;
            model.Memory = process.Memory;
            model.CpuUsage = oldModel is null
                ? null
                : GetCpuUsage(process.CpuTime - oldModel.CpuTotalTime,
                    process.TimeStamp - oldModel.CpuMeasurementTimeStamp);

            model.CpuTotalTime = process.CpuTime;
            model.CpuMeasurementTimeStamp = process.TimeStamp;
        }

        private static double? GetCpuUsage(TimeSpan cpuTimeDiff, TimeSpan timeDiff)
        {
            if (timeDiff == TimeSpan.Zero)
                return null;

            return cpuTimeDiff.TotalMilliseconds / timeDiff.TotalMilliseconds;
        }

        private void RunExecute()
        {
            // TODO: this method must be extracted to a separate service
            // TODO: View model must be created for RunCommandDialog
            RunCommandDialog runCommandDialog = new();
            runCommandDialog.Owner = Application.Current.MainWindow;
            bool? result = runCommandDialog.ShowDialog();
            if (result == true)
            {
                string commandLine = runCommandDialog.CommandTextBox.Text;
                if (string.IsNullOrWhiteSpace(commandLine))
                {
                    return;
                }

                List<string> args = Utils.SplitArgs(commandLine).ToList();
                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo(args[0], string.Join(" ", args.Skip(1)));
                    startInfo.UseShellExecute = true;
                    Process.Start(startInfo);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, LocalizationResources.ErrorCaption, MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        private void SwitchToAppExecute(object parameter)
        {
            if (parameter is ApplicationViewModel model)
            {
                ShowWindow(model.WindowHandle);
            }
        }

        private void KillProcessExecute(object parameter)
        {
            int processId;
            if (parameter is ProcessViewModel processViewModel)
            {
                processId = processViewModel.ProcessId;
            }
            else if (parameter is ApplicationViewModel applicationViewModel)
            {
                processId = applicationViewModel.ProcessId;
            }
            else
            {
                return;
            }

            try
            {
                Process process = Process.GetProcessById(processId);
                process.Kill();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, LocalizationResources.ErrorCaption, MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void ExitExecute()
        {
            // Stop updating lists
            _timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);

            // Save UI state
            _stateStorageService.SaveState(_viewModel.State);

            // Exit
            Application.Current.Shutdown();
        }

        private static void ShowWindow(nint windowHandle)
        {
            // TODO: extract as a service
            NativeInterop.WindowPlacement windowPlacement = default;
            windowPlacement.Length = Marshal.SizeOf<NativeInterop.WindowPlacement>();

            if (!NativeInterop.GetWindowPlacement(windowHandle, ref windowPlacement))
            {
                return;
            }

            // https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
            const int SW_NORMAL = 1;
            const int SW_SHOWMINIMIZED = 2;
            const int SW_SHOWMAXIMIZED = 3;
            const int SW_RESTORE = 9;

            switch (windowPlacement.ShowCmd)
            {
                case SW_SHOWMAXIMIZED:
                    NativeInterop.ShowWindow(windowHandle, SW_SHOWMAXIMIZED);
                    break;
                case SW_SHOWMINIMIZED:
                    NativeInterop.ShowWindow(windowHandle, SW_RESTORE);
                    break;
                default:
                    NativeInterop.ShowWindow(windowHandle, SW_NORMAL);
                    break;
            }

            NativeInterop.SetForegroundWindow(windowHandle);
        }
    }
}
