﻿namespace TaskManager.Services
{
    /// <summary>
    /// Service to load or save state.
    /// </summary>
    /// <typeparam name="TState"></typeparam>
    public interface IStateStorageService<TState>
        where TState : class
    {
        /// <summary>
        /// Loads state from a storage.
        /// </summary>
        /// <returns></returns>
        TState LoadState();

        /// <summary>
        /// Saves state to a storage.
        /// </summary>
        /// <param name="state"></param>
        void SaveState(TState state);
    }
}
