﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using TaskManager.Interops;
using TaskManager.Models;

namespace TaskManager.Services
{
    internal sealed class WindowListService : IWindowListService
    {
        public List<WindowModel> GetTopWindowList()
        {
            const int maxWindowTitleLength = 1024;

            // https://docs.microsoft.com/en-us/windows/win32/winmsg/window-styles
            const uint WS_VISIBLE = 0x10000000;

            // https://docs.microsoft.com/en-us/windows/win32/winmsg/extended-window-styles
            const uint WS_EX_APPWINDOW = 0x00040000;
            const uint WS_EX_TOOLWINDOW = 0x00000080;

            List<WindowModel> result = new();
            StringBuilder sb = new(maxWindowTitleLength + 1); // +1 for \0 termination character

            NativeInterop.EnumWindowProc enumWindowProc = (hwnd, param) =>
            {
                NativeInterop.WindowInfo info = default;
                info.Size = (uint)Marshal.SizeOf<NativeInterop.WindowInfo>();
                if (NativeInterop.GetWindowInfo(hwnd, ref info))
                {
                    // a window must be visible
                    if ((info.Style & WS_VISIBLE) != 0 &&
                        // a window must be in task bar and it must be not a tool window
                        // https://stackoverflow.com/questions/2125451/how-to-get-the-current-opened-applications-in-windows-using-c-or-vb-net
                        ((info.ExStyle & WS_EX_TOOLWINDOW) == 0 || (info.ExStyle & WS_EX_APPWINDOW) != 0))
                    {
                        int length = NativeInterop.GetWindowText(hwnd, sb, maxWindowTitleLength);

                        // a window must have non-empty title
                        if (length > 0)
                        {
                            string windowName = sb.ToString(0, length);
                            NativeInterop.GetWindowThreadProcessId(hwnd, out uint processId);
                            result.Add(new WindowModel
                            {
                                WindowHandle = hwnd,
                                Name = windowName,
                                ProcessId = (int)processId
                            });
                        }
                    }
                }

                return true;
            };

            NativeInterop.EnumWindows(enumWindowProc, 0);

            GC.KeepAlive(enumWindowProc);

            return result;
        }

        private static string GetModuleFileName(nint processId, StringBuilder sb)
        {
            int size = sb.Capacity - 1;
            int length;
            try
            {
                length = NativeInterop.GetModuleFileNameEx(processId, 0, sb, size);
            }
            catch (EntryPointNotFoundException)
            {
                length = NativeInterop.GetModuleFileNameExPsApi(processId, 0, sb, size);
            }

            return length == 0 ? string.Empty : sb.ToString(0, length);
        }
    }
}
