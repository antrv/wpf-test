﻿using System;
using System.Windows.Media.Imaging;
using TaskManager.Models;

namespace TaskManager.ViewModels
{
    public sealed class ProcessViewModel : NotifyPropertyChangedObject
    {
        private string _name;
        private int _processId;
        private long? _memory;
        private double? _cpuUsage;
        private int _threads;
        private BitmapSource _icon;

        public string Name
        {
            get => _name;
            set => SetPropertyValue(ref _name, value);
        }

        public int ProcessId
        {
            get => _processId;
            set => SetPropertyValue(ref _processId, value);
        }

        public long? Memory
        {
            get => _memory;
            set => SetPropertyValue(ref _memory, value);
        }

        public DateTime CpuMeasurementTimeStamp
        {
            get;
            set;
        }

        public TimeSpan CpuTotalTime
        {
            get;
            set;
        }

        public double? CpuUsage
        {
            get => _cpuUsage;
            set => SetPropertyValue(ref _cpuUsage, value);
        }

        public int Threads
        {
            get => _threads;
            set => SetPropertyValue(ref _threads, value);
        }

        public BitmapSource Icon
        {
            get => _icon;
            set => SetPropertyValue(ref _icon, value);
        }
    }
}
