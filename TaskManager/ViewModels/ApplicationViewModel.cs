﻿using System.Windows.Media.Imaging;
using TaskManager.Models;

namespace TaskManager.ViewModels
{
    public sealed class ApplicationViewModel : NotifyPropertyChangedObject
    {
        private string _name;
        private nint _windowHandle;
        private int _processId;
        private BitmapSource _icon;

        public string Name
        {
            get => _name;
            set => SetPropertyValue(ref _name, value);
        }

        public nint WindowHandle
        {
            get => _windowHandle;
            set => SetPropertyValue(ref _windowHandle, value);
        }

        public int ProcessId
        {
            get => _processId;
            set => SetPropertyValue(ref _processId, value);
        }

        public BitmapSource Icon
        {
            get => _icon;
            set => SetPropertyValue(ref _icon, value);
        }
    }
}
