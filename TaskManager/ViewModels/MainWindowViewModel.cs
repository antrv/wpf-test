﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using TaskManager.Models;

namespace TaskManager.ViewModels
{
    public sealed class MainWindowViewModel : NotifyPropertyChangedObject, IExitCommandProvider
    {
        private MainWindowState _state = new();
        private ICommand _runCommand;
        private ICommand _exitCommand;
        private ICommand _switchToAppCommand;
        private ICommand _killProcessCommand;

        /// <summary>
        /// Main Window state
        /// </summary>
        public MainWindowState State
        {
            get => _state;
            set => SetPropertyValue(ref _state, value);
        }

        /// <summary>
        /// Command to run a process.
        /// </summary>
        public ICommand RunCommand
        {
            get => _runCommand;
            set => SetPropertyValue(ref _runCommand, value);
        }

        /// <summary>
        /// Command to run a process.
        /// </summary>
        public ICommand ExitCommand
        {
            get => _exitCommand;
            set => SetPropertyValue(ref _exitCommand, value);
        }

        /// <summary>
        /// Moves the selected app main window to the foreground.
        /// </summary>
        public ICommand SwitchToAppCommand
        {
            get => _switchToAppCommand;
            set => SetPropertyValue(ref _switchToAppCommand, value);
        }

        /// <summary>
        /// Command to kill process.
        /// </summary>
        public ICommand KillProcessCommand
        {
            get => _killProcessCommand;
            set => SetPropertyValue(ref _killProcessCommand, value);
        }

        public ObservableCollection<ApplicationViewModel> Applications { get; } = new();

        public ObservableCollection<ProcessViewModel> Processes { get; } = new();
    }
}
