﻿using System.Windows;
using TaskManager.Models;

namespace TaskManager.ViewModels
{
    public sealed class MainWindowState : NotifyPropertyChangedObject
    {
        private double _width = 450;
        private double _height = 600;
        private WindowState _state = WindowState.Normal;

        /// <summary>
        /// Width of the Main Window
        /// </summary>
        public double Width
        {
            get => _width;
            set => SetPropertyValue(ref _width, value);
        }

        /// <summary>
        /// Height of the Main Window
        /// </summary>
        public double Height
        {
            get => _height;
            set => SetPropertyValue(ref _height, value);
        }

        /// <summary>
        /// State of the Main Window
        /// </summary>
        public WindowState WindowState
        {
            get => _state;
            set => SetPropertyValue(ref _state, value);
        }
    }
}
