﻿using System.Windows.Input;

namespace TaskManager.ViewModels
{
    public interface IExitCommandProvider
    {
        ICommand ExitCommand { get; }
    }
}
