﻿using System;
using System.Windows;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using TaskManager.Models;

namespace TaskManager
{
    internal static class Extensions
    {
        /// <summary>
        /// Registers window class of type <typeparamref name="TWindow"/> and view model of type <typeparamref name="TViewModel"/>.
        /// </summary>
        /// <typeparam name="TWindow"></typeparam>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="collection"></param>
        internal static void AddSingletonWindow<TWindow, TViewModel>([NotNull] this IServiceCollection collection)
            where TWindow : Window, new()
            where TViewModel : NotifyPropertyChangedObject
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            collection.AddSingleton<TViewModel>();

            collection.AddSingleton(serviceProvider =>
            {
                TViewModel viewModel = serviceProvider.GetRequiredService<TViewModel>();
                TWindow window = new TWindow();
                window.DataContext = viewModel;
                return window;
            });
        }
    }
}
